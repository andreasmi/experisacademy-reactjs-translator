# Experis Academy - ReactJs translator assignment

This is an assignment in ReactJs for the Noroff periode of Experis Academy

## Usage

Clone/fork repository. Run `npm install` to get the dependencies. To satrt the script, do `npm start`.
Go to http://localhost:3000 to view the assignent in the browser.

### Features

Both of the input boxes racts with both the enter key and a button. This was to ilustrate the `onKeyUp` attribut and the `event.keyCode` property. When you translate something, the program is going to store the last 10 queries. This queries can be found in the profile page by clicking the name in the top right corner. In the profile page you can either clear the list of translations or sign out. If you sign out, the list and the username will be deletede from localstorage. If you click the title of the webpage in the top left corner, you will be guided to the translation page.