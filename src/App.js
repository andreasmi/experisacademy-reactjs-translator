import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './components/Login';
import Profile from './components/Profile';
import Translate from './components/Translate';
import NotFound from './components/NotFound';

function App() {

  return (
    <Router>
      <div className="App">

          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/profile" component={Profile}/>
            <Route path="/translate" component={Translate}/>

            <Route path="*" component={NotFound} />
          </Switch>
          
      </div>
    </Router>
  );
}

export default App;
