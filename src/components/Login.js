import React, { useState } from 'react';
import Header from './shared/Header';
import { getProfile, setProfile } from '../utils/storage';
import { useHistory } from 'react-router-dom';

const Login = () => { 
    
    const history = useHistory();
    const profile = getProfile();

    const [ username, setUsername ] = useState('');
    
    const onUsernameChange = e => setUsername(e.target.value.trim());

    (function(){
        if(profile && profile!==''){
            history.push('/translate');
        }
    })();

    const checkInput = () => {
        if(username.length <= 0){
            return false;
        }
        return true;
    }

    const onKeyPressed = (e) => {
        if(e.keyCode === 13){
            if(checkInput()){
                setProfile({
                name: username});
                history.push('/translate');
            }
        }
    }

    const onButtonPressed = e => {
        if(checkInput()){
            setProfile({
                name: username
            });
            history.push('/translate');
        }
    }

    
    return (
        <div className="loginScreen">
            <Header profile={profile} />

            <input type="text" placeholder="Your name!" onKeyUp={onKeyPressed} onChange={onUsernameChange}/>
            <div className="loginButton" onClick={onButtonPressed}>Sign in</div>

        </div>
    );

}

export default Login;