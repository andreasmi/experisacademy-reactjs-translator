import React from 'react';
import Header from './shared/Header'
import { getProfile } from '../utils/storage';
import {Link} from 'react-router-dom';

const NotFound = () => {

    const profile = getProfile();

    return (
        <div>
            <Header profile={profile}/>
            <div className="notFoundWrapper">
                <h1 className="notFoundHeading">404 Not Found</h1>
                <p className="notFoundText">You landed on a page that doesn't exist! You has to be a wizard <span role="img" aria-label="Wizard icon">🧙‍♂️</span></p>
                <Link to="/translate"><div className="notFoundButton">Go to the real place</div></Link>
            </div>
        </div>
    )
};

export default NotFound;