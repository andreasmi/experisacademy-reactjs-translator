import React, { useState, useEffect } from 'react';
import Header from './shared/Header';
import { getProfile, getQueries, clearQueries, signOut } from '../utils/storage';
import { Redirect } from 'react-router-dom';



const Profile = (props) => { 

    const profile = getProfile();
    const queries = getQueries();
    
    const [ existingQueriesHasItems, setExistingQueriesHasItems ] = useState(false);
    const [ userSignedIn, setUserSignedIn ] = useState(true);

    useEffect(() => {
        setUserSignedIn(getProfile());
        console.log(getProfile());
    }, []);


    if(queries && !existingQueriesHasItems){
        setExistingQueriesHasItems(true);
    }

    let itemsToShow = [];
    if(existingQueriesHasItems){
        const queryData = queries.data.map((value, index) => {
            const classes = index%2===0?'profileLi profileLiLight':'profileLi profileLiDark';
            return <li className={classes} key={index}>{value}</li>;
        });
        itemsToShow.push(queryData);
    }


    const clearExistingQueries = () => {
        console.log("Clearing");
        clearQueries();
        setExistingQueriesHasItems(false);
    }


    const signOutUser = () => {
        console.log("signOutUser");
        clearExistingQueries();
        signOut();
        setUserSignedIn(false);
    }

    return (
        <div>
            {!userSignedIn && <Redirect to='/'/>}
            <Header profile={profile}/>

            <div className="profileButtonGroup">
                <p className="profileButton" onClick={clearExistingQueries}>Clear</p>
                <p className="profileButton" onClick={signOutUser}>Clear & sign out</p>
            </div>

            {existingQueriesHasItems && 
            <div>
                <h4 className="lastTranslationText">Your 10 last translations:</h4>
                <ul className="profileUl">
                    {itemsToShow}

                </ul>
            </div>}

        </div>
    );

}

export default Profile;