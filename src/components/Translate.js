import React, { useState } from 'react';
import Header from './shared/Header';
import { getProfile, addToQueries } from '../utils/storage';
import { useHistory } from 'react-router-dom';
import logo from '../assets/logo.png';
import logoHello from '../assets/logoHello.png';
import LoadingImage from '../assets/LoadingImage.gif';

const Translate = () => { 
    
    const profile = getProfile();
    const history = useHistory();

    //useStates
    const [ signs, setSigns ] = useState([]);
    const [ error, setError ] = useState('');
    const [ hasError, setHasError ] = useState(false);
    const [ somethingToTranslate, setSomethingToTranslate ] = useState(false);
    const [ stringToTranslate, setStringToTranslate ] = useState('');
    const [ talking, setTalking ] = useState(false);
    const [ loading, setLoading ] = useState(false);


    //If someone is trying to reach the translate page without having registered a profile name
    (function(){
        if(!profile || profile===''){
            history.push('/');
        }
    })();

    //Creating array for placement in spritesheet
    const placements = [];
    for(let i = 0; i < 4; i++){
        for(let j = 0; j < 8; j++){
            const x = String(j*150);
            const y = String(i*150);
            placements.push({
                x,
                y
            });
        }
    }

    //Regex for legal characters
    const legalAlphabet = /[a-z ]/;

    const inputChange = e => setStringToTranslate(e.target.value.trim());

    const keyDown = e => {
        if(e.keyCode === 13){
            return;
        }
        setTalking(true);
    }

    const translate = data => {
        setTalking(false);
        if(data.keyCode === 13 || data.type === 'click'){
            setLoading(true);
            const userInput = stringToTranslate;
            if(userInput === ''){
                setSigns([]);
                setLoading(false);
                setHasError(false);
                return;
            }
            const dataArray = userInput.toLowerCase().split('');
            const indexArray = [];
            dataArray.forEach(e => indexArray.push(e.charCodeAt(0)-97));
            setHasError(false);

            setSigns(indexArray.map((value, index) => {
                const letterIndex = value;
                if(!dataArray[index].match(legalAlphabet)){
                    setHasError(true);
                    setError('The translator will only translate characters from a-z');
                    return false;
                }
                setSomethingToTranslate(true);
                if(letterIndex === -65 ){
                    //If it is a space
                    return <div className="signLetter" key={index}>{dataArray[index]}</div>
                }
                //Shfting background
                const style = {
                    backgroundPositionX: `-${placements[letterIndex].x}px`, 
                    backgroundPositionY: `-${placements[letterIndex].y}px`
                };
                return <div className="sign" key={index} style={style}></div>;
            }));

            addToQueries(userInput);
            setLoading(false);
        }
    }

    return (
        <div>
            <Header profile={profile}/>
            <div className="translateInput">
                <div className="talkingLogo">
                    {!talking && <img src={logo} alt="logo"/>}
                    {talking && <img src={logoHello} alt="logo"/>}
                </div>
                <input className="translateInputElement" defaultValue='' type="text" placeholder="Sentance to translate" onKeyUp={translate} onKeyDown={keyDown} onChange={inputChange}/>
                <button className="btnTranslate" onClick={translate}>Translate</button>
                {hasError && <p className="error">{error}</p>}
            </div>
            {loading && <img className="loadingImage" src={LoadingImage} alt="Loading gif"/>}
            {somethingToTranslate && <div className="translated">
                {signs}
            </div>}
        </div>
    )

}

export default Translate;