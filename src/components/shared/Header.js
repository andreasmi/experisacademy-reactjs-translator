import React from 'react';
import { getProfile } from '../../utils/storage';
import ProfileBadge from './ProfileBadge';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';


const Header = () => {

    const profile = getProfile();
    const history = useHistory();

    const returnToTranslator = () => {
        if(history.location.pathname === '/profile'){
            history.goBack();
        }
    }

    return (
        <div className="profile">
            <h4 className="linkToHome" onClick={returnToTranslator}>
                The Translatooor
            </h4>
            <Link to="/profile" className="profileName">
                {profile && <ProfileBadge profile={profile}/>}
            </Link>
            
        </div>
    );
}

export default Header;