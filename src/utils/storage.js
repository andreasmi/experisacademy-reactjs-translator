export const setProfile = profile => localStorage.setItem('profile', JSON.stringify(profile));
export const getProfile = () => {
    const data = localStorage.getItem('profile');
    if(!data){
        return false;
    }
    return JSON.parse(data);
}
export const addToQueries = item => {
    if(!localStorage.getItem('queries')){
        localStorage.setItem('queries', JSON.stringify({
            data: []
        }));
    }

    const existingQueries = JSON.parse(localStorage.getItem('queries'));
    if(existingQueries.data.length === 10){
        existingQueries.data.shift();
    }
    existingQueries.data.push(item);
    localStorage.setItem('queries', JSON.stringify(existingQueries));
    console.log(localStorage.getItem('queries'))
    console.log(JSON.parse(localStorage.getItem('queries')).data.length)
}
export const getQueries = () => {
    const data = localStorage.getItem('queries');
    if(!data){
        return false;
    }
    return JSON.parse(data);
}
export const clearQueries = () => {
    localStorage.setItem('queries', '');
}
export const signOut = () => {
    localStorage.setItem('profile', '');
}